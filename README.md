NAME
====



`Geuse::Markdown` - Add Markdown rendering capability to the static site generator [Geuse](https://gitlab.com/uzluisf/geuse).

DESCRIPTION
===========



`Geuse::Pod` is only a plugin-wrapper around [`Text::Markdown`](https://github.com/retupmoca/p6-markdown) to facilitate the rendering of Markdown-based documents in Geuse.

Therefore, if you want to use Markdown as your markup language in Geuse, you must install this module or some other module that provides a similar capability.

For more information on how Geuse picks up the plugin, look at Geuse's [readme page](https://gitlab.com/uzluisf/geuse).

INSTALLATION
============



Using zef:
----------

    zef update && zef install Geuse::Markdown

From source:
------------

    git clone https://gitlab.com/uzluisf/geuse-markdown.git
    cd geuse-markdown && zef install .

DEPENDENCIES
============



This modules depends on [`Markit`](https://gitlab.com/uzluisf/raku-markit). If it's not installed on your system, install it locally with

    zef install Markit

AUTHOR
======



Luis F. Uceta

