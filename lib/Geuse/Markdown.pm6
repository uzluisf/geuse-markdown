unit module Geuse::Markdown;
use Markit;

sub make-markdowner( --> Sub ) {
    sub render-markdown( $raw-content ) {
        return Markdown.new.markdown($raw-content);
    }
    return &render-markdown
}

#| Register the Markdown plugin specified in the YAML file.
our sub register( $context is rw, %plugin-config --> Nil ) {
    =begin comment
    - $context is an instance of the GeuseContext class.
    - $plugin-config is a hash that contains the plugin's name and
      the extensions it renders. For instance, %(name => 'Geuse::Markdown',
      extensions => ['md', 'mkd', 'markdown']).
    =end comment

    # add new renderer to content renderer object.
    my @markdown-extentions = <md mkd markdown>;
    my &markdowner = make-markdowner();
    $context.content-renderer.add-renderer(@markdown-extentions, &markdowner);
}
