=begin pod

=NAME

C«Geuse::Markdown» - Add Markdown rendering capability to the static
site generator L«Geuse|https://gitlab.com/uzluisf/geuse».

=DESCRIPTION

C«Geuse::Pod» is only a plugin-wrapper around 
L«C«Text::Markdown»|https://github.com/retupmoca/p6-markdown»
to facilitate the rendering of Markdown-based documents in Geuse.

Therefore, if you want to use Markdown as your markup language in Geuse,
you must install this module or some other module that provides a similar
capability.

For more information on how Geuse picks up the plugin, look at
Geuse's L«readme page|https://gitlab.com/uzluisf/geuse».

=INSTALLATION

=head2 Using zef:
=code zef update && zef install Geuse::Markdown

=head2 From source:
=begin code
git clone https://gitlab.com/uzluisf/geuse-markdown.git
cd geuse-markdown && zef install .
=end code

=DEPENDENCIES

This modules depends on
L«C«Markit»|https://gitlab.com/uzluisf/raku-markit». 
If it's not installed on your system, install it locally with

=begin code
zef install Markit
=end code

=AUTHOR

Luis F. Uceta

=end pod
